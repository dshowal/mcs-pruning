/*
 * ModifiedGAPruner.h
 *
 *  Created on: Feb 2, 2011
 *      Author: rgreen
 */

#ifndef MODIFIEDGAPRUNER_H_
#define MODIFIEDGAPRUNER_H_

#include "GAPruner.h"

class ModifiedGAPruner: public GAPruner {
	public:
		ModifiedGAPruner(int popSize, int generations, Classifier* o, std::vector<Generator> g, std::vector<Line> l, double p, bool ul=false);
		ModifiedGAPruner(int popSize, int generations, double mut, double cross, Classifier* o, std::vector<Generator> g, std::vector<Line> l,
							double p, bool ul=false);
		virtual ~ModifiedGAPruner();
		void Prune(MTRand& mt);
};

#endif /* MODIFIEDGAPRUNER_H_ */
